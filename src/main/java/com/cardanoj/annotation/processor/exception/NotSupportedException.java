package com.cardanoj.annotation.processor.exception;

public class NotSupportedException extends Throwable {
    public NotSupportedException(String msg) {
        super(msg);
    }
}
